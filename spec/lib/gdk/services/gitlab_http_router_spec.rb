# frozen_string_literal: true

describe GDK::Services::GitLabHttpRouter do # rubocop:disable RSpec/FilePath
  describe '#name' do
    it { expect(subject.name).to eq('gitlab-http-router') }
  end

  describe '#command' do
    it 'returns the necessary command to run gitlab-http-router' do
      expect(subject.command).to eq('support/exec-cd gitlab-http-router npx wrangler dev -e dev --port 9393 --var GITLAB_PROXY_HOST:127.0.0.1:3000')
    end
  end

  describe '#enabled?' do
    it 'is disabled by default' do
      expect(subject.enabled?).to be(false)
    end
  end
end
