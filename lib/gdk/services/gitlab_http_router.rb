# frozen_string_literal: true

module GDK
  module Services
    class GitLabHttpRouter < Base
      def name
        'gitlab-http-router'
      end

      def command
        config.gitlab_http_router.__service_command
      end

      def enabled?
        config.gitlab_http_router.enabled?
      end
    end
  end
end
